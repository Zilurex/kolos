#include "stdafx.h"
#include "deck.h"
#include "gameplay.h"
#include <ctime>
#include <iostream>
#include <windows.h>
//#include <ncurses.h>
using namespace std;


void gotoxy(int x, int y)
{
  COORD coord;
  coord.X = x;
  coord.Y = y;
  SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}
void color(int c)
{
	HANDLE  hConsole;
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, c);
}

void card::display()
	{
		switch (this->value)
		{
	case 14:
		cout << "As ";
		break;
	case 13:
		cout << "Krol ";
		break;
	case 12:
		cout << "Dama ";
		break;
	case 11:
		cout << "Walet ";
		break;
	default:
		cout << this->value << " ";
		break;
	}

		switch (this->kolor)
		{
		case 0:
			cout << "pik" << endl;
			break;
		case 1:
			cout << "kier" << endl;
			break;
		case 2:
			cout << "karo" << endl;
			break;
		case 3:
			cout << "trefl" << endl;
			break;
		}
	}
	
	
	
	card::card()
{
	state = 0;
	visible = 0;
	color = 0;
}


	//konstruktor
	deck::deck()
	{
		HEAD = NULL;
		TAIL = NULL;
		counter=0;
		int x = 0;
		y = 0;
	}
	//destruktor
	deck::~deck()
	{
		//cout << "Odpalam ~deck()" << endl;
		/*card * p;

		while (HEAD)
		{
			p = HEAD->next;
			delete HEAD;
			HEAD = p;
		}*/
	}


	card * deck::push_front(card *p)
	{
		p->next = HEAD;
		p->prev = NULL;
		if (HEAD) HEAD->prev = p;
		HEAD = p;
		if (!TAIL) TAIL = HEAD;
		counter++;
		return HEAD;
	}
	
	card * deck::erase(card * p)//usuwa z listy elemetu 
	{

		if(p->prev) p->prev->next = p->next;
		else HEAD = p->next;
		if(p->next) p->next->prev = p->prev;
		else TAIL = p->prev;
		counter--;
		return p;
	} 

	card * deck::push_back(card * p)
	{
		if (TAIL) TAIL->next = p;
		p->next = NULL; p->prev = TAIL;
		TAIL = p;
		if (!HEAD) HEAD = TAIL;
		counter++;
		return TAIL;
	}

	card* deck::pop_front() //zabiera z poczatku
	{
		card * p, *p1;
		if (!HEAD)
		{
			cout << "Blad, lista jest pusta \n";
			return NULL;
		}
		else
		{	
			if(!(HEAD->next))
		    {
			
			HEAD->prev = NULL;
			HEAD->next = NULL;
			p1 = HEAD;
			HEAD= NULL;
			TAIL=NULL;
			counter--;
			return p1;
			}
			else
			{
			p = HEAD->next;
			p->prev = NULL;
			HEAD->prev = NULL;
			HEAD->next = NULL;
			p1 = HEAD;
			HEAD = p;
			counter--;
			return p1;
			}
		}
	}

	card* deck::pop_back() //zabiera z konca
	{
		card * p, *p1;
		if (!TAIL)
		{
			cout << "Blad! ";
			return NULL;
		}
		if(!(TAIL->prev))
		{
			TAIL->prev = NULL;
			TAIL->next = NULL;
			p1 = TAIL;
			HEAD= NULL;
			TAIL=NULL;
			counter--;
			return p1;
		}
		else
		{
			p = TAIL->prev;
			p->next = NULL;
			TAIL->prev = NULL;
			TAIL->next = NULL;
			p1 = TAIL;
			TAIL = p;
			counter--;
			return p1;
		}
	}

	card * deck::insert(card *p, card *behind)  // wstawianie p za behind
	{
		/*p->next = behind->next;
		if (behind->next)
			behind->next->prev = p;
		else
			TAIL = p;
		behind->next = p;
		p->prev = behind;
		counter++;
		return p;*/
		if (behind != NULL && p != NULL)
		{
			p->next = behind->next; p->prev = behind;
			behind->next = p;
			if (p->next) p->next->prev = p;
			else TAIL = p;
			counter++;
			return p;
		}
		else
		{
			cout << "\nInsert error!!\n";
			return NULL;
		}
		//if ()
	}

	unsigned deck::size()
	{
		return counter;
	}
	
	card* deck::index(unsigned n) // numerowanie kart od 1 nie od 0
	{
		card* p;
		//if (n <= 0 || n>counter)
		if ((n<1) || (n > counter))
		{
			cout << "\n Index error! \n";
			return NULL;
		}
		else if (n == counter) return TAIL;
		else if (n < counter / 2) // szukanie w dolnej polowie
		{
			p = HEAD;
			while (--n) p = p->next;
			return p;
		}
		else // szukanie w gornej polowie
		{
			p = TAIL;
			while (counter > n++) p = p->prev;
			return p;
		}
	}

	card * deck::pick(card * p)
	{
		if (p != NULL)
		{
			//card * p1;
			if (p->prev) p->prev->next = p->next;
			else HEAD = p->next;

			if (p->next) p->next->prev = p->prev;
			else TAIL = p->prev;
			counter--;
			return p;
		}
		else
		{
			cout << "\nPicking error! \n";
			return NULL;
		}
	}

	//generowanie talii
	void deck::genDeck()
	{
		card* p;
		for (int i = 1; i < 14; i++)
		{
			p = new card;
			p->value = i;
			p->kolor = card::pik;
			deck::push_front(p);
		}

		for (int i = 1; i < 14; i++)
		{
			p = new card;
			p->value = i;
			p->kolor = card::kier;
			deck::push_front(p);
		}

		for (int i = 1; i < 14; i++)
		{
			p = new card;
			p->value = i;
			p->kolor = card::karo;
			deck::push_front(p);
		}

		for (int i = 1; i < 14; i++)
		{
			p = new card;
			p->value = i;
			p->kolor = card::trefl;
			deck::push_front(p);
		}
	}

	void deck::shuffle()
	{
		srand(time(NULL));
		int n,z;
		for (int i = 0; i < 1000; i++)
		{
			n = rand() % 52 + 1;
			do
			{
				z = rand() % 52 + 1;
			} while (z == n);
			//cout << n << " " << z << endl;
			//p=this->pick(this->index(z));
			//this->pick(this->index(2));
			this->insert(pick(this->index(n)),this->index(z));
		}
	}
	


	void deck::displayUp()
	{
		int x = this->x;
		int y = this->y;

		if (HEAD)
		{

			card*q;
			q = HEAD;

			if (q->visible == 0)
			{
				gotoxy(x, y);
				cout << "|-----|" << endl;

				for (int i = 0; i<4; i++)
				{
					y += 1;
					gotoxy(x, y);
					cout << "|     |" << endl;
				}
				y += 1;
				gotoxy(x, y);
				cout << "|-----|" << endl;
			}

			if (q->visible == 1)
			{
				if ((q->kolor == 1) || (q->kolor == 2))
				{
					color(4);
				}
				if (q->color == 2)
					color(2);
				gotoxy(x, y);
				cout << "|-----|" << endl;

				y += 1;
				gotoxy(x, y);
				cout << "|";
				switch (q->value)
				{
				case 1:
					cout << "A ";
					break;
				case 13:
					cout << "K ";
					break;
				case 12:
					cout << "D ";
					break;
				case 11:
					cout << "W ";
					break;
				default:
					if (q->value>9)
						cout << q->value;
					else
						cout << q->value << " ";
					break;
				}
				switch (q->kolor)
				{
				case 0:
					cout << "**";
					break;
				case 1:
					cout << "<3";
					break;
				case 2:
					cout << "<>";
					break;
				case 3:
					cout << "-%";
					break;
				}
				cout << " |" << endl;

				for (int i = 0; i<3; i++)
				{
					y += 1;
					gotoxy(x, y);
					cout << "|     |" << endl;
				}

				y += 1;
				gotoxy(x, y);
				cout << "|-----|" << endl;
				color(7);
			}//if visible 1
		}//if HEAD
		else
		{
			gotoxy(x, y);
			cout << "/xxxxx\\" << endl;

			for (int i = 0; i<4; i++)
			{
				y += 1;
				gotoxy(x, y);
				cout << "x     x" << endl;
			}

			y += 1;
			gotoxy(x, y);
			cout << "\\xxxxx/" << endl;
		}
		xh = x;
		yh = y;
		color(7);
	}






	void deck::displayDown(deck reka)
	{
		/*deck *p;
		p = &reka;
		if (prev_marker != marker)
		{
			if (!reka.HEAD)
			{
				if (this != p)
					this->HEAD->visible = 1;
			}
		}*/
		int x = this->x;
		int y = this->y;
		int j = 0;
		gotoxy(x, y);
		for (int i = 0; i < 30; i++)
		{
			cout << "       ";
			gotoxy(x,y+i);

		}
			
		if (HEAD)
		{
			card*q  = this->TAIL;;
			//q = TAIL;
			
			for (unsigned i = 0; this->counter>i; i++)
			{
				if (q->color == 1)
					color(2);
				y = y + j;
				if (q->visible == 0)
				{
					gotoxy(x, y);
					cout << "|-----|" << endl;


					for (int i = 0; i<4; i++)
					{
						y += 1;
						gotoxy(x, y);
						cout << "|     |" << endl;
					}
					y += 1;
					gotoxy(x, y);
					cout << "|-----|" << endl;
				}

				if (q->visible == 1)
				{
					if ((q->kolor == 1) || (q->kolor == 2))
					{
						color(4);
					}
					if (q->color == 2)
						color(2);
					gotoxy(x, y);
					cout << "|-----|" << endl;

					y += 1;
					gotoxy(x, y);
					cout << "|";
					switch (q->value)
					{
					case 1:
						cout << "A ";
						break;
					case 13:
						cout << "K ";
						break;
					case 12:
						cout << "D ";
						break;
					case 11:
						cout << "W ";
						break;
					default:
						if (q->value>9)
							cout << q->value;
						else
							cout << q->value << " ";
						break;
					}
					switch (q->kolor)
					{
					case 0:
						cout << "**";
						break;
					case 1:
						cout << "<3";
						break;
					case 2:
						cout << "<>";
						break;
					case 3:
						cout << "-%";
						break;
					}
					cout << " |" << endl;


					for (int i = 0; i<3; i++)
					{
						y += 1;
						gotoxy(x, y);
						cout << "|     |" << endl;
					}

					y += 1;
					gotoxy(x, y);
					cout << "|-----|" << endl;
					color(7);
				}//if
				q = q->prev;
				j = -3;
			}//for
		}
		else
		{
			gotoxy(x, y);
			cout << "/xxxxx\\" << endl;

			for (int i = 0; i<4; i++)
			{
				y += 1;
				gotoxy(x, y);
				cout << "x     x" << endl;
			}

			y += 1;
			gotoxy(x, y);
			cout << "\\xxxxx/" << endl;
		}
		xh = x;
		yh = y;
		color(7);
		j = 0;
	}

	void deck::displayDown2(deck reka)
	{
		/*deck *p;
		p = &reka;
		if (prev_marker != marker)
		{
		if (!reka.HEAD)
		{
		if (this != p)
		this->HEAD->visible = 1;
		}
		}*/
		int x = this->x;
		int y = this->y;
		int j = 0;
		gotoxy(x, y);
		for (int i = 0; i < 30; i++)
		{
			cout << "       ";
			gotoxy(x, y + i);

		}

		if (HEAD)
		{
			card*q;
			q = TAIL;
			q = this->HEAD;
			for (unsigned i = 0; this->counter>i; i++)
			{
				if (q->color == 1)
					color(2);
				y = y + j;
				if (q->visible == 0)
				{
					gotoxy(x, y);
					cout << "|-----|" << endl;


					for (int i = 0; i<4; i++)
					{
						y += 1;
						gotoxy(x, y);
						cout << "|     |" << endl;
					}
					y += 1;
					gotoxy(x, y);
					cout << "|-----|" << endl;
				}

				if (q->visible == 1)
				{
					if ((q->kolor == 1) || (q->kolor == 2))
					{
						color(4);
					}
					if (q->color == 2)
						color(2);
					gotoxy(x, y);
					cout << "|-----|" << endl;

					y += 1;
					gotoxy(x, y);
					cout << "|";
					switch (q->value)
					{
					case 1:
						cout << "A ";
						break;
					case 13:
						cout << "K ";
						break;
					case 12:
						cout << "D ";
						break;
					case 11:
						cout << "W ";
						break;
					default:
						if (q->value>9)
							cout << q->value;
						else
							cout << q->value << " ";
						break;
					}
					switch (q->kolor)
					{
					case 0:
						cout << "**";
						break;
					case 1:
						cout << "<3";
						break;
					case 2:
						cout << "<>";
						break;
					case 3:
						cout << "-%";
						break;
					}
					cout << " |" << endl;


					for (int i = 0; i<3; i++)
					{
						y += 1;
						gotoxy(x, y);
						cout << "|     |" << endl;
					}

					y += 1;
					gotoxy(x, y);
					cout << "|-----|" << endl;
					color(7);
				}//if
				q = q->prev;
				j = -3;
			}//for
		}
		else
		{
			gotoxy(x, y);
			cout << "/xxxxx\\" << endl;

			for (int i = 0; i<4; i++)
			{
				y += 1;
				gotoxy(x, y);
				cout << "x     x" << endl;
			}

			y += 1;
			gotoxy(x, y);
			cout << "\\xxxxx/" << endl;
		}
		xh = x;
		yh = y;
		color(7);
		j = 0;
	}



	void deck::setvisible()
	{
		card *p;
		p = HEAD;
		p->visible = 1;
	}

	void deck::setColor(int ile_kart)
	{
		if (HEAD)
		{
			card *p;
			p = HEAD;
			for (int i = 0; ile_kart>i; i++)
			{
				p->color = 1;
				p = p->prev;
			}
		}
	}

	void deck::delateColor(int ile_kart)
	{
		if (HEAD)
		{
			card *p;
			p = HEAD;
			for (int i = 0; ile_kart>i; i++)
			{
				p->color = 0;
				p = p->prev;
			}
		}
	}

	void deck::setxy(int x, int y)
	{
		this->x = x;
		this->y = y;
		if (HEAD)
		{
			card *p;
			p = HEAD;
			p->visible = 1;
		}
	}

	
void deck::display()
	{
		card * p;
		int i = 1;
		if (!HEAD) cout << "Lista jest pusta." << endl;
		else
		{
			p = TAIL;
			while (p)
			{
				cout << "Karta nr:" << i << "  ";
				switch (p->value)
				{
				case 1:
					cout << "A";
					break;
				case 13:
					cout << "K";
					break;
				case 12:
					cout << "D";
					break;
				case 11:
					cout << "W";
					break;
				default:
					cout << p->value;
					break;
				}
				switch (p->kolor)
				{
				case 0:
					cout << " **";
					break;
				case 1:
					cout << " <3";
					break;
				case 2:
					cout << " <>";
					break;
				case 3:
					cout << " -%";
					break;
				}
				p = p->prev;
				i++;
				cout << endl;
			}
			cout << endl;
		}
	}

void deck::move()
{

}